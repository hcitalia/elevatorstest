﻿using UnityEngine;
using UnityEngine.UI;

public class LedButton : Button
{
    public Image ledImage;

    public void LedOn()
    {
        Color color = ledImage.color;
        color.a = 255f;
        ledImage.color = color;
    }

    public void LedOff()
    {
        Color color = ledImage.color;
        color.a = 0f;
        ledImage.color = color;
    }

    public override void ButtonOff()
    {
        LedOff();
    }

    public override void ButtonPress()
    {
        LedOn();
    }
}
