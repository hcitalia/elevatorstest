﻿using UnityEngine;

public abstract class Button : MonoBehaviour
{ 
    public virtual void ButtonOff()
    {

    }
    public virtual void ButtonPress()
    {

    } 
}
