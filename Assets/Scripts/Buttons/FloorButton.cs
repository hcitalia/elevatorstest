﻿using UnityEngine;
using UnityEngine.UI;

public class FloorButton : LedButton
{
    public int floor;
    public Text buttonText;
    
    public ElevatorAction elevatorAction;

    public void SetText(string newText)
    {
        buttonText.text = newText;
    }

    public override void ButtonOff()
    {
        base.ButtonOff();
    }

    public override void ButtonPress()
    {
        base.ButtonPress();
        ElevatorRequest elevatorRequest = new ElevatorRequest
        {
            elevatorAction = elevatorAction,
            floor = floor,
            priority = elevatorAction == ElevatorAction.None ? Priority.High : Priority.Low,
        };
        ElevatorManager.Instance.AddRequest(elevatorRequest);
    }
}
