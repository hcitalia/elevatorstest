﻿using System.Collections.Generic;
using System.Linq;

public class PickupPassengersOnCurrentDirection : ElevatorAlgorithm
{
    //ездит подбирая попутчиков
    public ElevatorAction GetNextAction(List<ElevatorRequest> ElevatorManager, ElevatorAction currentDirection, int currentFloor)
    {
        if (ElevatorManager.Count == 0)
        {
            return ElevatorAction.None;
        }
       
        var destinationArrivedRequests = ElevatorManager.Where(er => er.floor == currentFloor && 
                                                                      (er.priority == Priority.High || 
                                                                      (er.priority == Priority.Low && er.elevatorAction == currentDirection)));
        if(destinationArrivedRequests.Count() > 0)
        {            
            return ElevatorAction.OpenDoors;
        }

        var currentDirectionRequests = currentDirection == ElevatorAction.MoveUp ? ElevatorManager.Where(er => er.floor > currentFloor) : ElevatorManager.Where(er => er.floor < currentFloor);
        if(currentDirectionRequests.Count() == 0)
        {
            return ElevatorAction.SwitchDirection;
        } else
        {
            return currentDirection;
        }
    }
}
