﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Floor : MonoBehaviour
{
    public FloorButton buttonUp;
    public FloorButton buttonDown;
    public Text tFloor;
}
