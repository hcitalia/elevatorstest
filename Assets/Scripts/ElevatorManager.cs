﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ElevatorManager : MonoBehaviour, RefreshDisplay
{
    public static ElevatorManager Instance;
    public List<ButtonsPanel> interfacePanels;

    [SerializeField]
    private ElevatorController elevatorController;
    private ElevatorAlgorithm elevatorAlgorithm = new PickupPassengersOnCurrentDirection();
    private List<ElevatorRequest> elevatorRequests = new List<ElevatorRequest>();

    private void Awake()
    {
        if(Instance != null)
        {
            Destroy(this);
            return;
        }
        Instance = this;
    }

    public ElevatorAction GetAction(ElevatorAction currentAction, int currentFloor)
    {
        var request = elevatorAlgorithm.GetNextAction(elevatorRequests, currentAction, currentFloor);
        return request;
    }

    private void RemoveRequest(ElevatorRequest request)
    {
        elevatorRequests.Remove(request);
    }

    public void AddRequest(ElevatorRequest request)
    {
        if (!elevatorRequests.Contains(request))
        {
            elevatorRequests.Add(request);
            if (elevatorRequests.Count == 1 ||
                          (request.priority == Priority.High && elevatorController.GetElevatorState() == ElevatorState.Stopped))
            {

                elevatorController.ProceedNextTask();
            }
        }
    }

    public void RequestCompleted(ElevatorAction currentDirection, int currentFloor)
    {
        var requests = elevatorRequests.Where(er => er.floor == currentFloor && (er.priority == Priority.High || er.elevatorAction == currentDirection)).ToList();
        foreach (ElevatorRequest request in requests)
        {
            foreach(ButtonsPanel bp in interfacePanels)
            {
                bp.ButtonOff(request.floor, request.elevatorAction);
            }
            RemoveRequest(request);
        }
    }

    public void LiftStopped()
    {
        foreach (ElevatorPanel ep in interfacePanels.OfType<ElevatorPanel>())
        {
            ep.ButtonOff();
            break;
           
        }
        var highPriorityRequests = elevatorRequests.Where(er => er.priority == Priority.High).ToList();
        foreach (ElevatorRequest elevatorRequest in highPriorityRequests)
        {
            RemoveRequest(elevatorRequest);
        }
    }

    public void LedOpenDoor(bool isLedOn)
    {
        foreach (ElevatorPanel ep in interfacePanels.OfType<ElevatorPanel>())
        {
            if (isLedOn)
            {
                ep.bOpenDoors.LedOn();
            }
            else
            {
                ep.bOpenDoors.LedOff();
            }
        }
    }

    public void LedCloseDoor(bool isLedOn)
    {
        foreach (ElevatorPanel ep in interfacePanels.OfType<ElevatorPanel>())
        {
            if (isLedOn)
            {
                ep.bCloseDoors.LedOn();
            }
            else
            {
                ep.bCloseDoors.LedOff();
            }
        }
    }

    public void Display(int elevatorFloor, bool isDoorsOpened = false, ElevatorAction elevatorAction = ElevatorAction.None)
    {
        foreach (ButtonsPanel buttonsPanel in interfacePanels)
        {
            buttonsPanel.Display(elevatorFloor, isDoorsOpened, elevatorAction);
        }
    }
}
