﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FullDisplay : ElevatorDisplay
{
    [SerializeField]
    private Text tMoveDirection;

    public void SetMoveDirectionText(string newText)
    {
        tMoveDirection.text = newText;
    }
}
