﻿using UnityEngine;
using UnityEngine.UI;

public abstract class ElevatorDisplay : MonoBehaviour
{
    [SerializeField]
    private Text tFloor;
    [SerializeField]
    private Text tDoorsState;

    public void SetFloorText(string newText)
    {
        tFloor.text = newText;
    }

    public void SetDoorsStateText(string newText)
    {
        tDoorsState.text = newText;
    }
}
