﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Settings : MonoBehaviour
{
    public static Settings Instance;
    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(this);
            return;
        }
        Instance = this;
        DontDestroyOnLoad(this);
    }

    public float ElevatorMoveSpeed = 1.0f;
    public float DoorsSpeed = 1.0f;
    public float DoorsWaitTimeBeforeClose = 3.0f;
    public int floorsNum = 20;
}
