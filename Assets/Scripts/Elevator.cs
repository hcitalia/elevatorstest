﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class Elevator: MonoBehaviour
{
    
    public ElevatorController elevatorController;
    public int CurrentFloor { get; private set; }
    public ElevatorAction ElevatorDirection { get; private set; }
    public bool IsDoorsOpened { get; private set; }
    public ElevatorState state { get; private set; }

    private IEnumerator movingCoroutine;

    protected void Start()
    {
        CurrentFloor = 1;
        ElevatorDirection = ElevatorAction.None;
        IsDoorsOpened = false;
        state = ElevatorState.Idle;
        elevatorController.Display(CurrentFloor, IsDoorsOpened, ElevatorDirection);
    }

    private void OpenDoors()
    {
        StopAllCoroutines();
        StartCoroutine(WaitOpenDoors());
    }

    private void DoorsOpened()
    {
        IsDoorsOpened = true;
        elevatorController.Display(CurrentFloor, IsDoorsOpened, ElevatorDirection);
        ElevatorManager.Instance.LedOpenDoor(false);
        StopAllCoroutines();
        StartCoroutine(WaitDoors());
    }

    private void DoorsClosed()
    {
        IsDoorsOpened = false;
        elevatorController.Display(CurrentFloor, IsDoorsOpened, ElevatorDirection);
        ElevatorManager.Instance.LedCloseDoor(false);
        state = ElevatorState.Idle;
        elevatorController.ProceedNextTask();
    }

    
    private void ElevatorArrivedFloor()
    {
        elevatorController.Display(CurrentFloor, IsDoorsOpened, ElevatorDirection);
        state = ElevatorState.Idle;
        elevatorController.ProceedNextTask();
    }

    private IEnumerator WaitDoors()
    {
        yield return new WaitForSecondsRealtime(Settings.Instance.DoorsWaitTimeBeforeClose);
        CloseDoors();
    }

    private IEnumerator WaitOpenDoors()
    {
        yield return new WaitForSecondsRealtime(Settings.Instance.DoorsSpeed);
        DoorsOpened();
    }

    private IEnumerator WaitCloseDoors()
    {
        yield return new WaitForSecondsRealtime(Settings.Instance.DoorsSpeed);
        DoorsClosed();
    }

    private IEnumerator MovingElevator()
    {
        yield return new WaitForSecondsRealtime(Settings.Instance.ElevatorMoveSpeed);
        if(ElevatorDirection == ElevatorAction.MoveUp)
        {
            CurrentFloor++;
        }
        else
        {
            CurrentFloor--;
        }
        ElevatorArrivedFloor();
    }

    public void MoveElevator(ElevatorAction elevatorDirection) 
    {
        if (!IsDoorsOpened)
        {
            state = ElevatorState.Busy;
            ElevatorDirection = elevatorDirection;
            movingCoroutine = MovingElevator();
            StartCoroutine(movingCoroutine);
            elevatorController.Display(CurrentFloor, IsDoorsOpened, ElevatorDirection);
        }
    }

    public void StopElevator()
    {
        if(movingCoroutine != null)
        {
            StopCoroutine(movingCoroutine);
        }
        state = ElevatorState.Stopped;
        ElevatorManager.Instance.LiftStopped();
    }

    public void tryOpenDoors(bool byUser = false)
    {
        if (state == ElevatorState.Busy)
        {
            Debug.Log("Нельзя открывать двери на ходу!");
            return;
        }
        else if (state == ElevatorState.Stopped)
        {
            Debug.Log("Нельзя открывать двери когда лифт стоит между этажами!");
            return;
        }

        if (byUser)
        {
            ElevatorManager.Instance.LedOpenDoor(true);
        }
        OpenDoors();
    }

    public void CloseDoors(bool byUser = false)
    {
        if (!IsDoorsOpened)
        {
            Debug.Log("Двери и так закрыты");
            return;
        }

        if (byUser)
        {
            ElevatorManager.Instance.LedOpenDoor(false);
            ElevatorManager.Instance.LedCloseDoor(true);
        }

        StopAllCoroutines();
        StartCoroutine(WaitCloseDoors());
    }

    public void SwitchDirection()
    {
        ElevatorDirection = (ElevatorDirection == ElevatorAction.MoveUp) || (ElevatorDirection == ElevatorAction.None) ? ElevatorAction.MoveDown : ElevatorAction.MoveUp;
        elevatorController.ProceedNextTask();
    }
}
