﻿using UnityEngine;

public class ElevatorController : MonoBehaviour, ElevatorControlling, RefreshDisplay
{
   //здесь при необходимости реализуются например включение/выключение лифта, сброс...
    [SerializeField]
    private Elevator elevator;
    
    public void StopPressed()
    {
        elevator.StopElevator();
    }

    public void OpenDoorsPressed()
    {
        elevator.tryOpenDoors(true);
    }

    public void CloseDoorsPressed()
    {
        elevator.CloseDoors(true);
    }

    public void ProceedNextTask()
    {
        ElevatorAction elevatorAction = ElevatorManager.Instance.GetAction(elevator.ElevatorDirection, elevator.CurrentFloor);
        switch (elevatorAction)
        {
            case ElevatorAction.None:
                Display(elevator.CurrentFloor, false, ElevatorAction.None);
                break;
            case ElevatorAction.OpenDoors:
                elevator.tryOpenDoors();
                TaskCompleted();
                break;
            case ElevatorAction.SwitchDirection:
                elevator.SwitchDirection();
                break;
            default:
                elevator.MoveElevator(elevatorAction);
                break;
        }
    }

    public ElevatorState GetElevatorState()
    {
        return elevator.state;
    }

    public void TaskCompleted()
    {
        ElevatorManager.Instance.RequestCompleted(elevator.ElevatorDirection, elevator.CurrentFloor);
    }

    public void Display(int elevatorFloor, bool isDoorsOpened = false, ElevatorAction elevatorAction = ElevatorAction.None)
    {
        ElevatorManager.Instance.Display(elevatorFloor, isDoorsOpened, elevatorAction);
    }
}
