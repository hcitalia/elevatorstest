﻿using System.Collections.Generic;
using UnityEngine;

public abstract class ButtonsPanel : MonoBehaviour
{
    protected ElevatorDisplay elevatorDisplay;
    protected List<Button> buttons { get; private set; } = new List<Button>();

    public virtual void Display(int elevatorFloor, bool isDoorsOpened = false, ElevatorAction elevatorAction = ElevatorAction.None)
    {
        elevatorDisplay.SetFloorText(elevatorFloor.ToString());
        elevatorDisplay.SetDoorsStateText(isDoorsOpened ? "Открыты" :"Закрыты");
    }
    public virtual void ButtonOff(int floor = 0, ElevatorAction elevatorAction = ElevatorAction.None)
    {

    }
}
