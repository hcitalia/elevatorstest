﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ElevatorPanel : ButtonsPanel, IDragHandler, IBeginDragHandler, IEndDragHandler
{
    
    public FloorButton floorButtonPrefab;
    public GameObject viewContent;
    public LedButton bOpenDoors;
    public LedButton bCloseDoors;
    public FullDisplay fDisplay;
   

    private void Start()
    {
       elevatorDisplay = fDisplay;
       for (int i = Settings.Instance.floorsNum; i >= 1; i--)
        {
            FloorButton fb = Instantiate(floorButtonPrefab, viewContent.transform);
            fb.floor = i;
            fb.elevatorAction = ElevatorAction.None;
            fb.SetText(i.ToString());
            buttons.Add(fb);
        }
    }

    public override void ButtonOff(int floor = 0, ElevatorAction elevatorAction = ElevatorAction.None)
    {
        if (floor == 0)
        {
            foreach (FloorButton button in buttons)
            {
                button.ButtonOff();
            }
        }
        else
        {
            foreach (FloorButton fb in buttons)
            {
                if (fb.floor == floor)
                {
                    fb.ButtonOff();
                    break;
                }
            }
        }
    }

    public override void Display(int elevatorFloor, bool isDoorsOpened = false, ElevatorAction elevatorAction = ElevatorAction.None)
    {
        string tAction = "";
        switch (elevatorAction)
        {
            case ElevatorAction.MoveDown:
                tAction = "Вниз";
                break;
            case ElevatorAction.MoveUp:
                tAction = "Вверх";
                break;
            default:
                tAction = "Стоит";
                break;
        }
        fDisplay.SetMoveDirectionText(tAction);
        base.Display(elevatorFloor, isDoorsOpened);
    }

    public void OnDrag(PointerEventData eventData)
    {
        //что-то тащим
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        Debug.Log("Начали тащить");
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Debug.Log("Закончили тащить");
    }
}
