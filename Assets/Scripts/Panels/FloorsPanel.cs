﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorsPanel : ButtonsPanel
{
    public MiniDisplay mDisplay;
    public Floor floorPrefab;
    public GameObject viewContent;

    private void Start()
    {
        elevatorDisplay = mDisplay;
        for (int i = Settings.Instance.floorsNum; i >= 1; i--)
        {
            Floor floor = Instantiate(floorPrefab, viewContent.transform);
            floor.buttonUp.floor = i;
            floor.buttonDown.floor = i;
            floor.tFloor.text = i.ToString();
            buttons.Add(floor.buttonUp);
            buttons.Add(floor.buttonDown);
            if(i == 1)
            {
                floor.buttonDown.gameObject.SetActive(false);
            }
            if(i == Settings.Instance.floorsNum)
            {
                floor.buttonUp.gameObject.SetActive(false);
            }
        }
    }

    public override void ButtonOff(int floor = 0, ElevatorAction elevatorAction = ElevatorAction.None)
    {
        foreach (FloorButton fb in buttons)
        {
            if (fb.floor == floor && fb.elevatorAction == elevatorAction)
            {
                fb.ButtonOff();
                break;
            }
        }
    }

    public override void Display(int elevatorFloor, bool isDoorsOpened = false, ElevatorAction elevatorAction = ElevatorAction.None)
    {
        base.Display(elevatorFloor, isDoorsOpened);
    }
}

