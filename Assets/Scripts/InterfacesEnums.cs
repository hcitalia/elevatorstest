﻿using System.Collections.Generic;

public enum ElevatorAction
{
    MoveUp,
    MoveDown,
    None,
    OpenDoors,
    SwitchDirection
}

public enum Priority
{
    High,
    Low
}

public enum ElevatorState
{
    Idle, Busy, Stopped
}

public class ElevatorRequest
{
    public int floor;
    public ElevatorAction elevatorAction;
    public Priority priority;
}

public interface RefreshDisplay
{
    void Display(int elevatorFloor, bool isDoorsOpened = false, ElevatorAction elevatorAction = ElevatorAction.None);
}

public interface ElevatorControlling
{
    void ProceedNextTask();
}

public interface ElevatorAlgorithm
{
    ElevatorAction GetNextAction(List<ElevatorRequest> elevatorRequests, ElevatorAction elevatorAction, int currentFloor);
}
