﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class MenuUI : MonoBehaviour
{
    [SerializeField]
    private Text tFloorsNum;

    void Start()
    {
        tFloorsNum.text = Settings.Instance.floorsNum.ToString();
    }

    public void FloorNumChanged()
    {
        int.TryParse(tFloorsNum.text, out Settings.Instance.floorsNum);
    }

    public void StartGame()
    {
        SceneManager.LoadScene("GameScene");
    }
}
